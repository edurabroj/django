from django.conf.urls import include, url
from views import *
urlpatterns = [
    url(r'^$',PersonaSearch, name='Persona-List'),
    url(r'^formulario/$',PersonaGetForm, name='Persona-Get-Form'),
    url(r'^guardar/$',PersonaSave, name='Persona-Save'),
    url(r'^eliminar/$',PersonaDelete, name='Persona-Delete'),
    url(r'^buscar/$',PersonaSearch, name='Persona-Search'),
    url(r'^limpiarTabla/$',PersonaClean, name='Persona-Clean'),
]