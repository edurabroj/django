from django import forms
from .models import *

class DateInput(forms.DateInput):
    input_type = 'date'

class PersonaForm(forms.ModelForm):
	class Meta:
		model=Persona
		fields='__all__'
		widgets = {
			'fnac': DateInput()
		}

		labels = {
			'fnac':'Fecha de Nacimiento',
		}		

	def __init__(self, *args, **kwargs):
		super(PersonaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()): 
			field.widget.attrs['class'] = 'form-control'
		self.fields["fnac"].label='Fecha de Nacimiento'
		self.fields["isActivo"].label='Activo'
		self.fields["isActivo"].widget.attrs["class"]=''
		self.fields["fnac"].widget.attrs["class"]='form-control datepicker'
		