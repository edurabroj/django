from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
import json, time
#from .models import Persona
# Create your views here.

# def PersonaList(request):
# 	personas=Persona.objects.all()
# 	return render(request,
# 				 'persona/index.html',
# 				 {"personas":personas}
# 				 )
def PersonaGetForm(request):
	if request.method=='GET':
		dic=request.GET
		form=PersonaForm()
		if len(dic)==1:
			print("editar")
			persona=Persona.objects.get(pk=dic['id_obj'])
			form=PersonaForm(instance=persona)
		return render(request,"persona/form.html",
				 {"form":form})

def PersonaSave(request):
	if request.method=='POST':	
		id_obj=request.GET['id_obj']
		if int(id_obj)==0:
			form=PersonaForm(request.POST)
		else:
			obj=Persona.objects.get(pk=id_obj)
			form=PersonaForm(request.POST, instance=obj)
		if form.is_valid():
			form.save()
			data={"ok":True}
			return HttpResponse(json.dumps(data),content_type='application/json')
		else:
			return render(request,"persona/form.html",
				 {"form":form})
	return redirect('Persona-List')

def PersonaDelete(request):
	if request.method=='POST':
		id_obj=request.POST['id_obj']
		Persona.objects.get(pk=id_obj).delete()
	return redirect('Persona-List')
	#return HttpResponse('')

def PersonaSearch(request):
	if request.method == "POST":
		#nombre = request.POST['nombre']
		dicPersona = request.POST
		if len(str(dicPersona['nombre'])) == 0:
			personas = Persona.objects.all()
		else:
			personas = Persona.objects.filter(nombres__contains = dicPersona['nombre'])		
		print(" Consulta: "+str(personas))
		time.sleep(.5)
		return render(request,'persona/_tabla.html', {"personas":personas})

	#return HttpResponse('')
	else:
		personas=Persona.objects.all()
		return render(request,
					 'persona/index.html',
					 {"personas":personas}
					 )

def PersonaClean(request):
	if request.method == 'POST':
		Persona.objects.all().delete()
	return redirect('Persona-List')
