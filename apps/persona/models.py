from django.db import models
from django.db.models import Model
# Create your models here.
class Persona(Model):
	nombres = models.CharField(max_length=200)
	apellidos = models.CharField(max_length=200)
	fnac = models.DateField()
	isActivo=models.BooleanField()
	descripcion = models.TextField(blank=True, null=True)

	def __unicode__(self):
		return self.nombres+' ' +self.apellidos