var id_obj=0;
var nombre="";
$("#btnNuevo").on("click", function() {
	id_obj=0;
	$("#modal").find('.modal-body').load('formulario/');
	$("#modal").find('.modal-title').html('<h2>Nuevo</h2>');
	// $("#modal").modal("show");
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;

}
var csrftoken = getCookie('csrftoken');

$("#btnGuardar").on("click", function() {
	$.post
	(
		"guardar/?id_obj="+id_obj, 
		$("#form").serialize()
	).done(function(data){
		if(data["ok"])
		{
			RecargarTabla();
			//location.reload();
		}
		else
		{
			$("#modal").find('.modal-body').html(data);
		}
		//$( "#modal" ).modal('hide');
		//location.reload();
	})
});

$(".btnBuscarPersona").click(function(event){
	event.preventDefault()
	nombre = $("#criterio").val()

	LoaderShow();
	RecargarTabla();
})

function RecargarTabla(){
	$('#modal').modal('hide')
	$.ajax({
		url : 'buscar/',
		type : 'post',
		data : {
			'nombre' : nombre,
			'csrfmiddlewaretoken':csrftoken
		},
		success : function(data){
			//alert(data)
			$("#divTabla").html(data)
			LoaderHide();
			MostrarOcultarBtnLimpiar();
		} 
	})
}

function MostrarOcultarBtnLimpiar() {
	if ($('#tabla >tbody >tr').length > 0){
		$('#btnLimpiarTabla').show();
    }
    else{
		$('#btnLimpiarTabla').hide();
	}
}


MostrarOcultarBtnLimpiar();


function LoaderShow(){
	$("#loader").show();
	$("#divTabla").hide();
}

function LoaderHide(){
	$("#loader").hide();
	$("#divTabla").show();
}


$("#loader").hide();


$("#btnLimpiarTabla").on("click", function() {
	id_obj=$(this).attr("nro");
	//alert(idpersona);
	bootbox.confirm({ 
	  size: "small",
	  message: "Se eliminarán todos los registros. ¿Continuar?", 
	  callback: function(result){
	  	console.log(result);
	  	if(result){
	  		$.post("limpiarTabla/",
			{
				csrfmiddlewaretoken:csrftoken
			}
			).done(function(data){
				RecargarTabla();
				//location.reload();
			})
	  	}
	  }
	})
});