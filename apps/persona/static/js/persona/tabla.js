$(".btnEliminar").on("click", function() {
	id_obj=$(this).attr("nro");
	//alert(idpersona);
	bootbox.confirm({ 
	  size: "small",
	  message: "Tas Seguro?", 
	  callback: function(result){
	  	console.log(result);
	  	if(result){
	  		$.post("eliminar/",
			{
				id_obj:id_obj,
				csrfmiddlewaretoken:csrftoken
			}
			).done(function(data){
				RecargarTabla();
				//location.reload();
			})
	  	}
	  }
	})
});

$(".btnEditar").on("click", function() {
	// event.preventDefault();
	id_obj=$(this).attr("nro");
	$("#modal").find('.modal-body').load('formulario/?id_obj='+id_obj);
	$("#modal").find('.modal-title').html('<h2>Editar</h2>');
	$("#modal").modal("show");
});