from django.conf.urls import include, url
from django.contrib import admin
from apps.persona import urls as url_persona

urlpatterns = [
    # Examples:
    # url(r'^$', 'repaso.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^persona/', include(url_persona)),
]
